const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }
    getOne(search) {
        if(search.fuel==='phoneNumber')
            return this.dbContext.find(item=>item.phoneNumber===search.value).value();
        else if(search.fuel==='email')
            return this.dbContext.find(item=>item.email===search.value).value();
        else if(search.fuel==='id')
            return this.dbContext.find(item=>item.id===search.value).value();
    }
}

exports.UserRepository = new UserRepository();