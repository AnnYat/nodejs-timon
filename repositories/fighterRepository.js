const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }
    getOne(search) {
        if(search.fuel==='name')
            return this.dbContext.find(item=>item.name===search.value).value();
        else  if(search.fuel==='id')
            return this.dbContext.find(item=>item.id===search.value).value();
    }
}

exports.FighterRepository = new FighterRepository();