const responseMiddleware = (req, res,next) => {
    if(!req.err){
            res.send(res.data);
    }else{
        console.log("Err");
        res.status(400).send('{\n' +
            '    error: true,\n' +
            '    message: `' +
            req.err+'`\n' +
            '}');
    }

}

exports.responseMiddleware = responseMiddleware;