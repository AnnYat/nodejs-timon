const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    if(req.method==='POST'){
        try{
            if(req.body.name&&req.body.power&&req.body.defense){
                if(!(req.body.power>1&&req.body.power<100)){
                    throw "Power is not in right range(1-100)!";
                }
                else if(!((req.body.defense>1&&req.body.defense<10))){
                    throw "Defense is not in right range(1-10)!";
                }
                else if(!(req.body.name.length>0)){
                    //console.log()
                    throw "Name is too short";
                }
                let fighdob = FighterService.search({fuel:'name',value:req.body.name});
                if(fighdob){
                    if(req.body.name.toLowerCase()===fighdob.name.toLowerCase()){
                        throw "Fighter exists!"
                    }
                }

                req.body.fighter=fighter;
                if(req.body.health)
                    req.body.fighter.health=req.body.health;
                req.body.fighter.name=req.body.name;
                req.body.fighter.power=req.body.power;
                req.body.fighter.defense=req.body.defense;
            }
            else{
                throw "Not all fields are inserted!"
            }
            next();
        }
        catch (err){
            res.err=err;
            res.data='{\n' +
                '    error: true,\n' +
                '    message: `' +
                err      +
                '`\n' +
                '}';
            res.status(400).send('{\n' +
                '    error: true,\n' +
                '    message: `' +
                err      +
                '`\n' +
                '}');
        }
        finally {

        }
    }
    else {
        next();
    }

}

const updateFighterValid = (req, res, next) => {
   if(req.method=='PUT'){
       try {
            req.body.fighter={updatedAt:''};
            if(req.body.name){
                if(!(req.body.name.length>0)){
                    throw "Name is too short";
                }
                let fighdob = FighterService.search({fuel:'name',value:req.body.name});
                if(fighdob){
                    if(req.body.name.toLowerCase()===fighdob.name.toLowerCase()){
                        throw "Fighter exists!"
                    }
                }
                req.body.fighter.name=req.body.name;
            }
           if(req.body.power){
               if(!(req.body.power>1&&req.body.power<100)){
                   throw "Power is not in right range(1-100)!";
               }
               req.body.fighter.power=req.body.power;
           }
           if(req.body.defense){
               if(!((req.body.defense>1&&req.body.defense<10))){
                   throw "Defense is not in right range(1-10)!";
               }
               req.body.fighter.defense=req.body.defense;
           }
           next();
       }catch (err){
           res.err = err;
           res.data='{\n' +
               '    error: true,\n' +
               '    message: `' +
               err +
               '`\n' +
               '}';
           res.status(400).send('{\n' +
               '    error: true,\n' +
               '    message: `' +
               err +
               '`\n' +
               '}');
       }
   }
   else{
       next();
   }

}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;