const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
    if(req.method==='POST'){
        try {
            if (req.body.firstName && req.body.lastName && req.body.phoneNumber && req.body.email && req.body.password) {
                if (req.body.password.length < 3) {
                    throw "Password is too short!"
                } else if (!req.body.email.endsWith("@gmail.com")) {
                    throw "EMail is not google hosted!"
                } else if (!(req.body.phoneNumber.length === 13 && req.body.phoneNumber.startsWith("+380"))) {
                    throw "Phone number is incorrect!"
                }
                let userDobEmail = UserService.search({fuel:'email',value:req.body.email});
                let userDobPhoneNumber = UserService.search({fuel: 'phoneNumber', value: req.body.phoneNumber});
                if(userDobEmail){
                    if(req.body.email.toLowerCase()=== userDobEmail.email.toLowerCase()){
                        throw "User exists!"
                    }
                }
                if (userDobPhoneNumber) {
                    if (req.body.phoneNumber.toLowerCase() === userDobPhoneNumber.phoneNumber.toLowerCase()) {
                        throw "User exists!"
                    }
                }
                req.body.user = user;
                req.body.user.firstName = req.body.firstName;
                req.body.user.email = req.body.email;
                req.body.user.phoneNumber = req.body.phoneNumber;
                req.body.user.lastName = req.body.lastName;
                req.body.user.password = req.body.password;
            } else {
                throw "Not all fields are inserted!"
            }
            next();
        } catch (err) {
            res.err = err;
            res.data='{\n' +
                '    error: true,\n' +
                '    message: `' +
                err +
                '`\n' +
                '}';
            res.status(400).send('{\n' +
                '    error: true,\n' +
                '    message: `' +
                err +
                '`\n' +
                '}');
        } finally {

        }
    }else {
    next();
    }



}

const updateUserValid = (req, res, next) => {
    if(req.method==='PUT'){
      try {
          req.body.user = {updatedAt:''};
          if(req.body.email){
              if (!req.body.email.endsWith("@gmail.com")) {
                  throw "EMail is not google hosted!"
              }
              let userDobEmail = UserService.search({fuel:'email',value:req.body.email});
              if(userDobEmail){
                  if(req.body.email.toLowerCase()=== userDobEmail.email.toLowerCase()){
                      throw "User exists!"
                  }
              }
              req.body.user.email=req.body.email;
          }
          if(req.body.phoneNumber){
              if (!(req.body.phoneNumber.length === 13 && req.body.phoneNumber.startsWith("+380"))) {
                  throw "Phone number is incorrect!"
              }
              let userDobPhoneNumber = UserService.search({fuel: 'phoneNumber', value: req.body.phoneNumber});
              if (userDobPhoneNumber) {
                  if (req.body.phoneNumber.toLowerCase() === userDobPhoneNumber.phoneNumber.toLowerCase()) {
                      throw "User exists!"
                  }
              }
              req.body.user.phoneNumber=req.body.phoneNumber;
          }
          if(req.body.password){
              if (req.body.password.length < 3) {
                  throw "Password is too short!"
              }
              req.body.user.password=req.body.password;
          }
          if(req.body.firstName){
              req.body.user.firstName=req.body.password;
          }
          if(req.body.lastName){
              req.body.user.lastName=req.body.lastName;
          }

          next();
      }
      catch (err){
          res.err = err;
          res.data='{\n' +
              '    error: true,\n' +
              '    message: `' +
              err +
              '`\n' +
              '}';
          res.status(400).send('{\n' +
              '    error: true,\n' +
              '    message: `' +
              err +
              '`\n' +
              '}');
      }
      finally{

      }

    }
    else{
        next();
    }



}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;