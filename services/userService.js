

const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    create(userdata){
        const item = UserRepository.create(userdata);
        if(!item) {
            return null;
        }
        return item;
    }
     update(id,assignData) {
         const item = UserRepository.update(id,assignData);
         if(!item){
             return null;
         }
         return item;
    }
    delete(id){
        const item = UserRepository.delete(id);
        if(!item){
            return null;
        }
        return item;
    }
    getAll(){
        const arr = UserRepository.getAll();
        if(!arr){
            return null;
        }
        return arr;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();