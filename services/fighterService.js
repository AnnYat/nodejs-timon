const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(userdata){
        const item = FighterRepository.create(userdata);
        if(!item) {
            return null;
        }
        return item;
    }
    update(id,assignData) {
        const item = FighterRepository.update(id,assignData);
        if(!item){
            return null;
        }
        return item;
    }
    delete(id){
        const item = FighterRepository.delete(id);
        if(!item){
            return null;
        }
        return item;
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        console.log(item);
        if(!item) {
            return null;
        }
        return item;
    }
    getAll(){
        const arr = FighterRepository.getAll();
        if(!arr){
            return null;
        }
        return arr;
    }
}

module.exports = new FighterService();