const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();



router.post('/login', (req, res, next) => {
    try {
        if(req.body.email&&req.body.password&&!res.err){
            res.status(200);
            let data = AuthService.login({email:req.body.email,password:req.body.password});
            if(!data){
                throw "No such user!"
            }
            res.data = data;
        }
        else{
            throw  "Invalid password or email!";
        }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;