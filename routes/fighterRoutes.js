const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

//router.get
router.use(createFighterValid);

router.post('/',(req,res,next)=>{

    try {
        res.data=FighterService.create(req.body.fighter);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }

},responseMiddleware);
router.get('/',(req,res,next)=>{
    try {
        res.data=FighterService.getAll();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.get('/:id',(req,res,next)=>{
    try {
        res.data=FighterService.search({value: req.params["id"], fuel: 'id'});
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.put('/:id',(req,res,next)=>{
    try {
        let id =req.params["id"];
        let user = FighterService.search({value: req.params["id"], fuel: 'id'});
        if(!user){
            res.data=FighterService.create(req.body.user);
            res.status(201);
        }
        else {
            res.data = FighterService.update(id, req.body.user);
            res.status(200);
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.delete('/:id',(req,res,next)=>{
    {
        try {
            let id =req.params["id"];
            let user = FighterService.search({value: req.params["id"], fuel: 'id'});
            if(!user){
                throw "No such user!"
            }
            else {
                res.data = FighterService.delete(id);
                res.status(200);
            }
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    }
},responseMiddleware)

module.exports = router;