const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();



router.use(updateUserValid);
router.use(createUserValid);


router.post('/',(req,res,next)=>{

    try {
        res.data=UserService.create(req.body.user);
        //res.send();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }

},responseMiddleware);

router.get('/',(req,res,next)=>{
    try {
        res.data=UserService.getAll();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.get('/:id',(req,res,next)=>{
    try {
        res.data=UserService.search({value: req.params["id"], fuel: 'id'});
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.put('/:id',(req,res,next)=>{
    try {
        let id =req.params["id"];
        let user = UserService.search({value: req.params["id"], fuel: 'id'});
        if(!user){
            res.data=UserService.create(req.body.user);
            res.status(201);
        }
        else {
            res.data = UserService.update(id, req.body.user);
            res.status(200);
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.delete('/:id',(req,res,next)=>{
    {
        try {
            let id =req.params["id"];
            let user = UserService.search({value: req.params["id"], fuel: 'id'});
            if(!user){
                throw "No such user!"
            }
            else {
                res.data = UserService.delete(id);
                res.status(200);
            }
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    }
},responseMiddleware)

module.exports = router;